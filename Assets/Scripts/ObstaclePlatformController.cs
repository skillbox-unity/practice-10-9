using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePlatformController : MonoBehaviour
{
    public float maxTimeToTurn;
    public float speed;

    private float currentTimeToTurn;
    private int left;

    private void Start()
    {
        left = -1;
        currentTimeToTurn = maxTimeToTurn;
    }

    private void Update()
    {
        if(currentTimeToTurn > 0)
        {
            currentTimeToTurn -= Time.deltaTime;
        }
        else
        {
            currentTimeToTurn = maxTimeToTurn;
            left *= -1;
        }
            
    }

    private void FixedUpdate()
    {
        Vector3 pos = new Vector3(Mathf.Cos(transform.eulerAngles.z) * left, Mathf.Sin(transform.eulerAngles.z) * left, 0);
        transform.position += pos * speed;
    }
}
