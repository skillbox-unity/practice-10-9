using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{

    private Rigidbody platformRigid;

    public int power;
    public float maxTimerForce;
    public float TimeToStart;

    private float currentTimeForce;

    // Start is called before the first frame update
    void Start()
    {
        platformRigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (TimeToStart > 0)
            TimeToStart -= Time.deltaTime;

        if (currentTimeForce > 0 && TimeToStart < 0)
            currentTimeForce -= Time.deltaTime;
    }

    private void FixedUpdate()
    {

        if (currentTimeForce > 0 && TimeToStart < 0)
            platformRigid.AddForce(Vector3.down * power * (maxTimerForce - currentTimeForce), ForceMode.Force);
            
    }

    public void StartSpring()
    {
        currentTimeForce = maxTimerForce;
    }
}
