using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public PlatformController platformController;

    private void OnTriggerEnter(Collider other)
    {
        platformController.StartSpring();
    }
}
