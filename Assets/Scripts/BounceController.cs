using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceController : MonoBehaviour
{
    public int power;

    private void OnCollisionEnter(Collision collision)
    {
        Vector3 direction = collision.transform.position - transform.position;
        collision.rigidbody.AddForce(direction.normalized * power, ForceMode.Impulse);
    }
}
